# AWS Cloud Formation

This cloud formation script will automate the process of creating a VPN Server within the AWS EC2 infrastructure in the region specified by the user. 

# AWS Console Login

The AWS Console can be accessed via the following link

[https://console.aws.amazon.com/console/](https://console.aws.amazon.com/console/)


## Create an AWS Account

An AWS account will be required to implement cloud formation. This process should be self explanatory. A credit card may be required. 

VPN server will use a t2.micro instance that may be eligible for a ‘free tier’ billing. In addition there will be monthly charges for the Elastic IP (Static IP) ~ $1/month. Costs will be incurred while the EC2 instance is running. It is recommended to stop the EC2 instance when not in use. This can be started via the GUI or CLI on demand. If the VPN instance is no longer required, it is recommended to delete the cloud formation instance as that should delete all resources. You can repeat this process to configure another instance. 

Cloud formation creation will take a 5-10 when started for all the resources to be created and configured. 

## Select Region

1. Click on the region from the upper right corner. 
2. Select your region you would like to deploy your VPN into

# Create SSH Key

An ssh key pair will be required for cloud formation to work. These keys will allow you to ssh into the EC2 t2.micro instance if needed, but not required. When you create your ssh keys, you will have **one **option to save the ssh key. After the key is created, you will not be able to download the private key again. 

Prior to creating your ssh key-pair, you will need to select your region first. 

1. Click **Services **from the top-left, and enter **EC2**

2. Click **Key pairs** to enter ssh key management

3. Click **Create key pair **from the upper-right to create a new key-pair. If you have existing keys, you can skip this step. You will need to note the name of the key-pair as this will be required later. 

4. Enter the **Name **for the key and select the desired key format. Click **Create Key Pair**

5. You will now see the configured key pair. 

# Create Cloud Formation 

1. Click **Services** from the top-left. 
2. Under **All Services,** enter “**Cloud Formation**”
3. Click “**Create Stack**”

## Create Stack
4. Under Specify Template, select Upload a template file
5. Click Choose file and select your yaml file. 
6. Click Next

## Specify stack details

7. Complete the form:

## Stack Name
*   Stack name: Name to uniquely identify the purpose/name of the VPN Server
    *   Test-VPN
*   ClientIPCIDR: 0.0.0.0/24	
    *   0.0.0.0/24 will allow the client to connect from any IP address
*   EasyRSAREQCN (Required Common Name in the VPN configuration)
    *   test-vpn-cn
*   EasyRSAREQCNAlgorithm (Algorithm for the vpn)
    *   Default: rsa
*   LatestAmiId (Amazon Linux ami)
    *   Default: (Will populate with the latest version)
*   OpenVpnPort (TCP or UDP Port for the VPN to listen on)
    *   Default: 1194
*   OpenVPNProtocol
    *   Default: udp
*   OpenVPNVersion (latest test at the file release 11/2019)
    *   Default: 2.4.7 
*   SSHKeyName (SSH Key that is stored in AWS to assign to this server)
    *   vpn-ssh-key     (This should match the key name created earlier)


## Configure stack options

*   No configuration needed. 

## Advanced options



*   No configuration needed. 
*   Click **Next**

## Review 

### Step 1. Specify template
*   No configuration needed. 

### Step 2: Specify stack details
*   No configuration needed. 

### Step 3: Configure stack options
*   No configuration needed. 

### Rollback configuration
*   No configuration needed. 

### Capabilities
*   Click “**I acknowledge…**”
*   Click **Create Stack**

## Stack Instantiation

This process will take a few minutes for all the resources to be created. Click through the tabs to view the resources. 

If an error occurs, review the tabs for details on the error. Click Delete to remove the stack if needed and repeat the process. 


### Creation started

### Creation in progress

### Creation Complete!

## Retrieve OpenVPN Configuration

### Select EC2 Service

1. Select **Services **from the top-left
2. Enter **s3 **under All Services
3. Click on vpn bucket
4. Click on **client/**
5. Click on .ovpn file to save

# Delete Cloud Formation Stack
1. Return to Cloud Formation by clicking **Services **and typing **Cloud Formation**
2. Click the **stack **to delete, and then click **Delete**

# Connect with your vpn client!
